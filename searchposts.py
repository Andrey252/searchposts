# -*- coding: utf-8 -*-
VERSION = '1.0 (09.02.2018)'
AUTHOR = 'Рыжкин Андрей'

from datetime import datetime
from requests import get as getResponse
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from textwrap import dedent
import json
import requests

INSTAGRAM_ACCESS_TOKEN = 'YOUR_INSTAGRAM_TOKEN'
VK_ACCESS_TOKEN = '83f224ae83f224ae83f224ae1c839307ec883f283f224aed98f6ea82f488992aa51a9ee'
FB_ACCESS_TOKEN = '1621163228000354|1aoaWt2jY3Ld6hQ9AW569Lz7UV8'
FB_APP_ID = '1621163228000354'
FB_APP_SECRET = '00bd11f154c984772dc25287ed717a67'

DISTANCE = '100'
TIME_INCREMENT = 60 * 60 * 24


def getFB(search):
    return getResponse('https://graph.facebook.com/v1.0/search?q=' + search + '&type=post&access_token=' + FB_ACCESS_TOKEN)


def getInstagram(latitude, longitude, distance, minTimestamp, maxTimestamp):
    params = {
        'lat': latitude,
        'lng': longitude,
        'distance': distance,
        'min_timestamp': str(minTimestamp),
        'max_timestamp': str(maxTimestamp),
        'access_token': INSTAGRAM_ACCESS_TOKEN
    }
    return getResponse("https://api.instagram.com/v1/media/search", \
                       params=params, verify=True).json()


def getVK(search, latitude, longitude, startTime, endTime):
    params = {
        'access_token': VK_ACCESS_TOKEN,
        'extended': '1',
        'q': search,
        'latitude': latitude,
        'longitude': longitude,
        'start_time': startTime,
        'end_time': endTime,
    }
    return getResponse("https://api.vk.com/method/newsfeed.search", \
                       params=params, verify=True).json()

def convertTSToDate(timestamp):
    return datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S') + ' UTC'


def parseInstagram(latitude, longitude, distance, minTimestamp, maxTimestamp, dateIncrement):
    if INSTAGRAM_ACCESS_TOKEN == 'YOUR_INSTAGRAM_TOKEN':
        print('You should add your instagram access token')
        return
    print('Parsing instagram..')
    fileDescriptor = open('instagram_' + latitude + longitude + '.html', 'w')
    fileDescriptor.write('<html>')
    localMinTimestamp = minTimestamp
    while (1):
        if (localMinTimestamp >= maxTimestamp):
            break
        localMaxTimestamp = localMinTimestamp + dateIncrement
        if (localMaxTimestamp > maxTimestamp):
            localMaxTimestamp = maxTimestamp
        print(convertTSToDate(localMinTimestamp), '-', convertTSToDate(localMaxTimestamp))
        responseJSON = getInstagram(latitude, longitude, distance, localMinTimestamp, localMaxTimestamp)
        for fieldJSON in responseJSON['data']:
            fileDescriptor.write('<br>')
            fileDescriptor.write('<img src=' + fieldJSON['images']['standard_resolution']['url'] + '><br>')
            fileDescriptor.write(convertTSToDate(int(fieldJSON['created_time'])) + '<br>')
            fileDescriptor.write(fieldJSON['link'] + '<br>')
            fileDescriptor.write('<br>')
        localMinTimestamp = localMaxTimestamp
    fileDescriptor.write('</html>')
    fileDescriptor.close()

def parseVK(search, latitude, longitude, minTimeStamp, maxTimeStamp, dateIncrement):
    print('Parsing search vkontakte..')
    fileDescriptor = open('vk_posts_' + latitude + longitude + '.txt', mode='w', encoding='utf-8')

    fileDescriptor.write('Заданные координаты: ' + latitude + ' ' + longitude + '\n' +
                         'Поиск:' + search + '\n\n')
    localMinTimestamp = minTimeStamp
    while (1):
        if (localMinTimestamp >= maxTimeStamp):
            break
        localMaxTimestamp = localMinTimestamp + dateIncrement
        if (localMaxTimestamp > maxTimeStamp):
            localMaxTimestamp = maxTimeStamp
        print(convertTSToDate(localMinTimestamp), '-', convertTSToDate(localMaxTimestamp))
        responseJSON = getVK(search, latitude, longitude, localMinTimestamp, localMaxTimestamp)

        for fieldJSON in responseJSON['response']:
            if type(fieldJSON) is int:
                continue
            photoJSON = fieldJSON['attachments'][0]
            fileDescriptor.write('Дата: ' + convertTSToDate(fieldJSON['date'])+ '\n' + 'Добавил: ' + fieldJSON['user']['first_name'] + ' ' + fieldJSON['user']['last_name'] + '\n')
            if (photoJSON['type'] == 'photo'):
             fileDescriptor.write('Фото поста: ' + photoJSON['photo']['src_big'] + '\n' +
                                  'Содержание: ' + fieldJSON['text'] + '\n' +
                                  'Лайки: ' + (str)(fieldJSON['likes']['count'])+ '\n' +
                                  'Тип post: ' + fieldJSON['post_type'] + '\n' +
                                'Координаты: ' + fieldJSON['geo']['coordinates'] + ' ' + fieldJSON['geo']['place']['title']+'\n\n')
            with open("vk_posts" + latitude + longitude + '.json', "w", encoding="utf-8") as file:
                json.dump(fieldJSON, file)

        localMinTimestamp = localMaxTimestamp

    fileDescriptor.close()



def getFbToken(app_id, app_secret):
    payload = {'grant_type': 'client_credentials', 'client_id': app_id, 'client_secret': app_secret}
    file = requests.post('https://graph.facebook.com/oauth/access_token?', params = payload)
    result = file.text.split("=")[0]
    return result

def main():
    global INSTAGRAM_ACCESS_TOKEN
    global FB_ACCESS_TOKEN
    global VK_ACCESS_TOKEN

    global DISTANCE
    global TIME_INCREMENT
    global VERSION
    global AUTHOR
    INFO = dedent('''
	-------- пример --------

	python searchposts.py 55.740701 37.609161 1400619600 1400792400 'поиск'

	-------- about ----------

	Parsing photos from Instagram and VK by geographic coordinates.
	Version: %s
	Author: %s

	-------- arguments ------
	''' % (VERSION, AUTHOR))
    parser = ArgumentParser(description=INFO,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("latitude", type=str,
                        help="Geographic latitude")
    parser.add_argument("longitude", type=str,
                        help="Geographic longitude")
    parser.add_argument("min_timestamp", type=int,
                        help="Start the search from this timestamp")
    parser.add_argument("max_timestamp", type=int,
                        help="Finish the search in this timestamp")
    parser.add_argument("search", type=str, help="Search #TAG")
    parser.add_argument("-d", "--distance", action="store",
                        help="Distance for search (meters). Default: %s" % DISTANCE)
    parser.add_argument("-i", "--increment", action="store",
                        help="Time increment for search (seconds). Default: %d" % TIME_INCREMENT)

    args = parser.parse_args()
    if args.distance:
        DISTANCE = args.distance
    if args.increment:
        TIME_INCREMENT = args.increment
    print('GEO:', args.latitude, args.longitude)
    print('TIME: from', convertTSToDate(args.min_timestamp), 'to', convertTSToDate(args.max_timestamp))
    print('DISTANCE: %s' % DISTANCE)
    print('TIME INCREMENT: %d' % TIME_INCREMENT)

    parseInstagram(args.latitude,
                   args.longitude,
                   DISTANCE,
                   args.min_timestamp,
                   args.max_timestamp,
                   TIME_INCREMENT)

    parseVK(args.search, args.latitude, args.longitude, args.min_timestamp, args.max_timestamp, TIME_INCREMENT)

if __name__ == "__main__":
    main()
