# pygraphml

[![Build Status](https://travis-ci.org/hadim/pygraphml.svg?branch=master)](https://travis-ci.org/hadim/pygraphml)
[![DOI](https://zenodo.org/badge/4163/hadim/pygraphml.svg)](https://zenodo.org/badge/latestdoi/4163/hadim/pygraphml)
[![PyPI version](https://img.shields.io/pypi/v/pygraphml.svg?maxAge=2591000)](https://pypi.org/project/pygraphml/)

`pygraphml` is a small Python library designed to parse GraphML file. To
see specification about GraphML, see http://graphml.graphdrawing.org/

Documentation and tutorial are available at http://hadim.github.io/pygraphml/. A notebook is also available [here](example.ipynb).

# Requirements

- Python > 2.7 and > 3.4
- NetworkX (http://networkx.lanl.gov/): only for the visualization

# Установка

1. Клонирование репозитория: git clone https://bitbucket.org/Andrey252/searchposts.git
2. Выполнить команду: python searchposts.py 55.740701 37.609161 1400619600 1400792400 hello (55.740701, 37.609161 - координаты; 1400619600 1400792400 - период; hello - поиск по постам

# Входные данные

1. Координаты;
2. Период по времени;
3. Строка поиска или хэштег.

# Выходные данные

1. txt данные о постах
2. json данные о постах